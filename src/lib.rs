#![deny(non_snake_case)]
//! Constants for dealing with midi events.
//! 
//! 
//! # Warning
//! If you use `midi-consts`, we recommend putting `#![deny(non_snake_case)]`
//! at the top in the main file of your crate.
//! 
//! 
//! Rust's mechanism for matching can be surprising in some examples:
//! ```
//! let x = 1;
//! match x {
//!     THIS_LOOKS_LIKE_A_CONSTANT => println!("Branch A"),
//!     _ => println!("Branch B"),
//! }
//! ```
//! If `THIS_LOOKS_LIKE_A_CONSTANT` is a constant that is in scope and that equals `2`,
//! then this code will print "Branch B".
//! However, if `THIS_LOOKS_LIKE_A_CONSTANT` is not defined as a constant or is not brought
//! into scope, a local variable with the name `THIS_LOOKS_LIKE_A_CONSTANT` will be defined
//! and the code will print "Brach A".
//! If this is not expected, you will only get the following error message:
//! ```text
//! warning: variable `THIS_LOOKS_LIKE_A_CONSTANT` should have a snake case name
//!  --> file.rs:4:9
//!   |
//! 4 |         THIS_LOOKS_LIKE_A_CONSTANT => println!("Branch a"),
//!   |         ^^^^^^^^^^^^^^^^^^^^^^^^^^ help: convert the identifier to snake case:  `this_looks_like_a_constant`
//!   |
//!   = note: `#[warn(non_snake_case)]` on by default
//! ```
//! This warning may get lost.
//! You can turn it into an error by adding `#![deny(non_snake_case)]`
//! at the top in the main file of your crate.


/// Constants for channel events
///
/// Channel events consist of two or three bytes:
/// * byte 1: event type (first four bits) and midi channel (last four bits)
/// * byte 2: parameter 1
/// * byte 3: parameter 2 (used for most, but not all event types)
pub mod channel_event {
    /// The first byte of a channel event contains both the event type
    /// and the midi channel in one byte.
    /// Use this bit mask to get the event type.
    ///
    /// ## Example
    /// ```
    /// use midi_consts::channel_event::{EVENT_TYPE_MASK, NOTE_OFF, NOTE_ON};
    /// # fn get_byte() -> u8 { 0x94}
    /// let byte: u8 = get_byte(); // Suppose we got this byte somewhere.
    /// let event_type = byte & EVENT_TYPE_MASK; // (binary AND)
    /// match event_type {
    ///     NOTE_OFF => { /* ... */ },
    ///     NOTE_ON => { /* ... */ },
    ///     // ...
    ///     _ => { /* ...*/ }
    /// }
    /// ```
    pub const EVENT_TYPE_MASK: u8 = 0b1111_0000;

    /// The first byte of a channel event contains both the event type
    /// and the midi channel in one byte.
    /// Use this bit mask to get the midi channel.
    ///
    /// ## Example
    /// ```
    /// use midi_consts::channel_event::MIDI_CHANNEL_MASK;
    /// # fn get_byte() -> u8 { 0x94}
    /// let byte: u8 = get_byte(); // Suppose we got this byte somewhere.
    /// let midi_channel: u8 = byte & MIDI_CHANNEL_MASK; // (binary AND)
    /// ```
    pub const MIDI_CHANNEL_MASK: u8 = 0b0000_1111;

    /// Event type of note off event.
    ///
    /// A channel event of this type is as follows:
    /// * byte 1: `NOTE_OFF | channel`
    /// * byte 2: note number (0-127)
    /// * byte 3: velocity (how hard the key was released)
    ///
    /// # Remark
    /// A note off event is often represented as a note on event
    /// with velocity `0`.
    pub const NOTE_OFF: u8 = 0x80;

    /// Event type of note on event.
    ///
    /// A channel event of this type is as follows:
    /// * byte one: `NOTE_ON | channel`
    /// * byte two: note number (0-127)
    /// * byte three: velocity (how hard the key was pressed)
    ///
    /// # Remark
    /// A note off event is often represented as a note on event
    /// with velocity `0`.
    pub const NOTE_ON: u8 = 0x90;

    /// Event type for polyphonic key pressure ("aftertouch").
    ///
    /// A channel event of this type is as follows:
    /// * byte one: `POLYPHONIC_KEY_PRESSURE | channel`, where `channel` is the channel (0-16)
    /// * byte two: note number (0-127)
    /// * byte three: aftertouch value
    pub const POLYPHONIC_KEY_PRESSURE: u8 = 0xA0;

    /// Event type of a controller event.
    ///
    /// A channel event of this type is as follows:
    /// * byte one: `CONTROL_CHANGE | channel`, where `channel` is the channel (0-16)
    /// * byte two: controller type (0-127)
    /// * byte three: new controller value
    ///
    /// See the [`control_change`] module for more details.
    ///
    /// [`control_change`]: ./control_change/index.html
    pub const CONTROL_CHANGE: u8 = 0xB0;

    /// Event type of a program change.
    ///
    /// A channel event of this type has only two bytes and is as follows:
    /// * byte one: `PROGRAM_CHANGE | channel`, where `channel` is the channel (0-16)
    /// * byte two: program number (0-127)
    pub const PROGRAM_CHANGE: u8 = 0xC0;

    /// Event type of channel pressure ("channel aftertouch").
    ///
    /// Events of this type are assumed to effect all currently
    /// playing notes for the specified channel.
    /// A channel event of this type has only two bytes and is as follows:
    /// * byte one: `CHANNEL_KEY_PRESSURE | channel`, where `channel` is the channel (0-16)
    /// * byte two: pressure amount (0-127)
    pub const CHANNEL_KEY_PRESSURE: u8 = 0xD0;

    /// Event type of a pitch bend event.
    ///
    /// A channel event of this type is as follows:
    /// * byte one: `PITCH_BEND_CHANGE | channel`, where `channel` is the channel (0-16)
    /// * byte two: least significant byte of the pitch bend amount (0-127)
    /// * byte three: most significant byte of the pitch bend amount
    ///
    /// By combining the two bytes that make up the pitch bend amount, you
    /// can get a `u16` value that describes the pitch bend amount.
    /// Value 8192 means "no pitch change", lower values mean decrease in pitch
    /// and higher values mean increase in pitch.
    pub const PITCH_BEND_CHANGE: u8 = 0xE0;

    /// Constants to represent controller change types.
    ///
    /// A control change channel event is as follows:
    /// * byte one: `CONTROL_CHANGE | channel`, where `channel` is the channel (0-16)
    /// * byte two: controller type (0-127). This module contains constants for these types.
    /// * byte three: new controller value
    ///
    /// # Remark
    /// Some control change types come in pairs: one with the most significant byte (MSB)
    /// and one with the least significant byte (LSB).
    pub mod control_change {
        const LSB_MASK: u8 = 0x20;

        /// Bank select: most significant byte.
        pub const BANK_SELECT_MSB: u8 = 0x00;

        /// Bank select: least significant byte.
        pub const BANK_SELECT_LSB: u8 = BANK_SELECT_MSB | LSB_MASK;

        /// Modulation: most significant byte.
        pub const MODULATION_MSB: u8 = 0x01;

        /// Modulation: least significant byte.
        pub const MODULATION_LSB: u8 = MODULATION_MSB | LSB_MASK;

        /// Breath controller: most significant byte.
        pub const BREATH_CONTROLLER_MSB: u8 = 0x02;

        /// Breach controller: least significant byte.
        pub const BREATH_CONTROLLER_LSB: u8 = BREATH_CONTROLLER_MSB | LSB_MASK;

        /// Foot controller: most significant byte.
        pub const FOOT_CONTROLLER_MSB: u8 = 0x04;

        /// Foot controller: least significant byte.
        pub const FOOT_CONTROLLER_LSB: u8 = FOOT_CONTROLLER_MSB | LSB_MASK;

        /// Portamento: most significant byte.
        pub const PORTAMENTO_TIME_MSB: u8 = 0x05;

        /// Portamento: least significant byte.
        pub const PORTAMENTO_TIME_LSB: u8 = PORTAMENTO_TIME_MSB | LSB_MASK;

        /// Data entry: most significant byte.
        pub const DATA_ENTRY_MSB: u8 = 0x06;

        /// Data entry: least significant byte.
        pub const DATA_ENTRY_LSB: u8 = DATA_ENTRY_MSB | LSB_MASK;

        /// Main volume: most significant byte.
        pub const MAIN_VOLUME_MSB: u8 = 0x07;

        /// Main volume: least significant byte.
        pub const MAIN_VOLUME_LSB: u8 = MAIN_VOLUME_MSB | LSB_MASK;

        /// Balance: most significant byte.
        pub const BALANCE_MSB: u8 = 0x08;

        /// Balance: least significant byte.
        pub const BALANCE_LSB: u8 = BALANCE_MSB | LSB_MASK;

        /// Pan: most significant byte.
        pub const PAN_MSB: u8 = 0x0A;

        /// Pan: least significant byte.
        pub const PAN_LSB: u8 = PAN_MSB | LSB_MASK;

        /// Expression controller: most significant byte.
        pub const EXPRESSION_CONTROLLER_MSB: u8 = 0x0B;

        /// Expression controller: least significant byte.
        pub const EXPRESSION_CONTROLLER_LSB: u8 = EXPRESSION_CONTROLLER_MSB | LSB_MASK;

        /// Effect control 1: most significant byte.
        pub const EFFECT_CONTROL_1_MSB: u8 = 0x0C;

        /// Effect control 1: least significant byte.
        pub const EFFECT_CONTROL_1_LSB: u8 = EFFECT_CONTROL_1_MSB | LSB_MASK;

        /// Effect control 2: most significant byte.
        pub const EFFECT_CONTROL_2_MSB: u8 = 0x0D;

        /// Effect control 2: least significant byte.
        pub const EFFECT_CONTROL_2_LSB: u8 = EFFECT_CONTROL_2_MSB | LSB_MASK;

        /// General purpose controller 1: most significant byte.
        pub const GENERAL_PURPOSE_CONTROLLER_1_MSB: u8 = 0x10;

        /// General purpose controller 1: least significant byte.
        pub const GENERAL_PURPOSE_CONTROLLER_1_LSB: u8 =
            GENERAL_PURPOSE_CONTROLLER_1_MSB | LSB_MASK;

        /// General purpose controller 2: most significant byte.
        pub const GENERAL_PURPOSE_CONTROLLER_2_MSB: u8 = 0x11;

        /// General purpose controller 2: least significant byte.
        pub const GENERAL_PURPOSE_CONTROLLER_2_LSB: u8 =
            GENERAL_PURPOSE_CONTROLLER_2_MSB | LSB_MASK;

        /// General purpose controller 3: most significant byte.
        pub const GENERAL_PURPOSE_CONTROLLER_3_MSB: u8 = 0x12;

        /// General purpose controller 3: least significant byte.
        pub const GENERAL_PURPOSE_CONTROLLER_3_LSB: u8 =
            GENERAL_PURPOSE_CONTROLLER_3_MSB | LSB_MASK;

        /// General purpose controller 4: most significant byte.
        pub const GENERAL_PURPOSE_CONTROLLER_4_MSB: u8 = 0x13;

        /// General purpose controller 4: least significant byte.
        pub const GENERAL_PURPOSE_CONTROLLER_4_LSB: u8 =
            GENERAL_PURPOSE_CONTROLLER_4_MSB | LSB_MASK;

        /// Damper pedal.
        pub const DAMPER_PEDAL: u8 = 0x40;

        /// Portamento.
        pub const PORTAMENTO: u8 = 0x41;

        /// Sustenuto.
        pub const SUSTENUTO: u8 = 0x42;

        /// Soft pedal.
        pub const SOFT_PEDAL: u8 = 0x43;

        /// Legato footswitch.
        pub const LEGATO_FOOTSWITCH: u8 = 0x44;

        /// Hold 2.
        pub const HOLD_2: u8 = 0x45;

        /// Sound controller 1. Default: Timber variation
        pub const SOUND_CONTROLLER_1: u8 = 0x46;

        /// Sound controller 2. Default: Timber/harmonic content
        pub const SOUND_CONTROLLER_2: u8 = 0x47;

        /// Sound controller 3. Default: Release time
        pub const SOUND_CONTROLLER_3: u8 = 0x48;

        /// Sound controller 4. Default: Attack time
        pub const SOUND_CONTROLLER_4: u8 = 0x49;

        /// Sound controller 5.
        pub const SOUND_CONTROLLER_5: u8 = 0x4A;

        /// Sound controller 6.
        pub const SOUND_CONTROLLER_6: u8 = 0x4B;

        /// Sound controller 7.
        pub const SOUND_CONTROLLER_7: u8 = 0x4C;

        /// Sound controller 8.
        pub const SOUND_CONTROLLER_8: u8 = 0x4D;

        /// Sound controller 9.
        pub const SOUND_CONTROLLER_9: u8 = 0x4E;

        /// Sound controller 10.
        pub const SOUND_CONTROLLER_10: u8 = 0x4F;

        /// General purpose controller 5: most significant byte.
        ///
        /// # Remark
        /// As far as I know, this has no LSB (least significant byte) variant.
        pub const GENERAL_PURPOSE_CONTROLLER_5_MSB: u8 = 0x50;

        /// General purpose controller 6: most significant byte.
        ///
        /// # Remark
        /// As far as I know, this has no LSB (least significant byte) variant.
        pub const GENERAL_PURPOSE_CONTROLLER_6_MSB: u8 = 0x51;

        /// General purpose controller 7: most significant byte.
        ///
        /// # Remark
        /// As far as I know, this has no LSB (least significant byte) variant.
        pub const GENERAL_PURPOSE_CONTROLLER_7_MSB: u8 = 0x52;

        /// General purpose controller 8: most significant byte.
        ///
        /// # Remark
        /// As far as I know, this has no LSB (least significant byte) variant.
        pub const GENERAL_PURPOSE_CONTROLLER_8_MSB: u8 = 0x53;

        /// Portamento.
        pub const PORTAMENTO_CONTROL: u8 = 0x54;

        /// Effects depth 1. Formerly "External Effects Depth"
        pub const EFFECTS_1_DEPTH: u8 = 0x5B;

        /// Effects depth 2. Formerly "Tremolo Depth"
        pub const EFFECTS_2_DEPTH: u8 = 0x5C;

        /// Effects depth 3. Formerly "Chorus Depth"
        pub const EFFECTS_3_DEPTH: u8 = 0x5D;

        /// Effects depth 4. Formerly "Celeste Detune"
        pub const EFFECTS_4_DEPTH: u8 = 0x5E;

        /// Effects depth 5. Formerly "Phaser Depth"
        pub const EFFECTS_5_DEPTH: u8 = 0x5F;

        /// Non-registered parameter number: least significant byte.
        pub const NON_REGISTERED_PARAMETER_NUMBER_LSB: u8 = 0x62;

        /// Non-registered parameter number: most significant byte.
        pub const NON_REGISTERED_PARAMETER_NUMBER_MSB: u8 = 0x63;

        /// Registered parameter number: least significant byte.
        pub const REGISTERED_PARAMETER_NUMBER_LSB: u8 = 0x64;

        /// Registered parameter number: most significant byte.
        pub const REGISTERED_PARAMETER_NUMBER_MSB: u8 = 0x65;

        /// Mode message: all sound off.
        ///
        /// For this event, the data byte (the third byte of the event) should be `0`
        pub const ALL_SOUND_OFF: u8 = 0x78;

        /// Mode message: reset all controllers.
        ///
        /// For this event, the data byte (the third byte of the event) should be `0`
        pub const RESET_ALL_CONTROLLERS: u8 = 0x79;

        /// Mode message: local control.
        ///
        /// When local control is on (default), the device responds to its local controls.
        /// When local control is off, it only responds to data recieved over MIDI.
        ///
        /// See the module [`local_control`] for possible values of the data byte
        /// (the third byte of the event).
        ///
        /// [`local_control`]: ./local_control/index.html
        pub const LOCAL_CONTROL: u8 = 0x7A;

        /// Constants for the data byte (3rd byte) of a local control control change event.
        pub mod local_control {
            /// Local control off: the device only responds to data recieved over MIDI.
            pub const LOCAL_CONTROL_OFF: u8 = 0;
            /// Local control on: the device also responds to local events (keys played, ...).
            pub const LOCAL_CONTROL_ON: u8 = 127;
        }

        /// Mode message: all notes off.
        ///
        /// For this event, the data byte (the third byte of the event) should be `0`.
        pub const ALL_NOTES_OFF: u8 = 0x7B;

        /// Mode message: omni mode off.
        ///
        /// For this event, the data byte (the third byte of the event) should be `0`.
        /// # Remark
        /// This message also causes all notes off.
        pub const OMNI_MODE_OFF: u8 = 0x7C;

        /// Mode message: omni mode on.
        ///
        /// For this event, the data byte (the third byte of the event) should be `0`.
        /// # Remark
        /// This message also causes all notes off.
        pub const OMNI_MODE_ON: u8 = 0x7D;

        /// Mode message: mono mode on
        ///
        /// For this event, the data byte (the third byte of the event)
        /// indicates the number of channels (omni off) or `0` (omni on).
        /// # Remark
        /// This message also causes all notes off.
        pub const MONO_MODE_ON: u8 = 0x7E;

        /// Poly mode on
        ///
        /// # Remark
        /// This message also causes all notes off.
        pub const POLY_MODE_ON: u8 = 0x7F;
    }
}

/// Constants for system events
pub mod system_event {
    /// System Exclusive message
    ///
    /// System exclusive messages have a variable length and are terminated by an [`EOX`].
    /// * byte one: `SYSEX`
    /// * byte two or byte two, three and four: manufacturer ID (assigned by MMA or AMEI)
    ///   or one of the predefined "Universal System Exclusive Message" bytes (see below)
    /// * following bytes: data bytes
    /// * last byte: [`EOX`]
    ///
    /// Some manufacturer ID's are one byte long.
    /// Other manufacturer ID's start with a zero byte ([`ID_EXTENSION`]) and are three bytes long.
    ///
    /// Instead of a manufacturer ID, the second byte can also contain a flag that
    /// indicates the start of a "Universal System Exclusive Message" (which is confusingly
    /// not manufacturer exclusive).
    /// See the module [`sysex`] for these universal system exclusive message.
    ///
    /// [`ID_EXTENSION`]: TODO
    /// [`sysex`]: TODO
    /// [`EOX`]: ./TODO
    pub const SYSEX: u8 = 0xF0;

    /// Content for the second byte of a system exclusive message,
    /// including "Universal System Exclusive Message".
    pub mod sysex {
        /// Indicates that the manufacturer ID is an extended ID, using three bytes.
        ///
        /// If the second byte of a System Exclusive message is `ID_EXTENSION`,
        /// then the manufacturer ID is three bytes long, using bytes two, three and four
        /// of the system exclusive message:
        /// * byte two of the system exclusive message: `ID_EXTENSION`,
        /// * byte three: extension group,
        /// * byte four: extended manufacturer ID
        pub const ID_EXTENSION: u8 = 0;

        /// Universal System Exclusive Message: non Real Time universal exclusive message
        ///
        /// * byte two of the system exclusive message: `NON_REAL_TIME`
        /// * byte three: device ID, where value `7F` ("all call") indicates all devices
        /// * byte four: Sub ID #1
        /// * byte five: Sub ID #2
        /// * remaining bytes before `EOX`: parameters
        pub const NON_REAL_TIME: u8 = 0x7e;

        /// Real Time universal exclusive message: realtime universal exclusive message
        ///
        /// * byte two of the system exclusive message: `REAL_TIME`
        /// * byte three: device ID, where value `7F` ("all call") indicates all devices
        /// * byte four: Sub ID #1
        /// * byte five: Sub ID #2
        /// * remaining bytes before `EOX`: parameters
        pub const REAL_TIME: u8 = 0x7f;

        /// Constants for Universal System Exclusive messages, both realtime and non realtime
        pub mod usysex {
            /// Send this for all devices to listen.
            pub const ALL_CALL: u8 = 0x7f;
        }
    }

    /// System common messages

    /// MTC Quarter Frame
    pub const MTC_QUARTER_FRAME: u8 = 241;

    /// Song position Pointer
    /// * byte two: LSB of the pointer
    /// * byte three: MSB of the pointer
    pub const SONG_POSITION_POINTER: u8 = 242;

    /// Song Select
    /// * byte two: Song # 0-127
    pub const SONG_SELECT: u8 = 243;

    /// Tune Request
    pub const TUNE_REQUEST: u8 = 246;

    /// End Of eXclusive message
    /// This is the last byte of the SysEx
    pub const EOX: u8 = 0xF7;

    /// System real time messages

    /// Timing Clock
    pub const RT_TIMING_CLOCK: u8 = 248;

    /// Start playback
    pub const RT_START: u8 = 250;
    /// Continue playback
    pub const RT_CONTINUE: u8 = 251;
    /// Stop playback
    pub const RT_STOP: u8 = 252;

    /// Active Sensing
    pub const RT_ACTIVE_SENSING: u8 = 254;

    /// Reset all
    pub const RT_SYSTEM_RESET: u8 = 255;
}
