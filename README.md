# Midi consts

A crate with constants for handling MIDI data, written in Rust.

Features
--------

Currently, only channel events are supported.
In order to spend developer time most effectively, we'll add other event types only as needed.
If you would like support for more, you can open an issue or send me an e-mail to the email address
in the commits.

Contributing
------------

We welcome contributions, both in the form of issues and in the form of pull requests.
Before opening a pull request, please open an issue first so that you know whether a subsequent
merge request would likely be approved.

If you don't have a Codeberg account, alternatively, you can contribute via e-mail (an email address
is in the commits). But just creating a Codeberg account is probably the easiest.

Related crates
--------------

* [`wmidi`](https://crates.io/crates/wmidi) provides data structures for handling midi events.

License
-------

`midi-consts` is distributed under the terms of the MIT license or the Apache License (Version 2.0), 
at your choice.
For the application of the MIT license, the examples included in the doc comments are not
considered "substantial portions of this Software".

Unless explicitly stated otherwise, you agree that your contributions are licensed as described
above.
